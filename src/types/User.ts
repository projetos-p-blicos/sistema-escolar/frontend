export type IUser = {
    id?: string;
    name?: string;
    email?: string;
    username?: string;
    roles?: IRole[];
    active?: string;
    created_at?: string;
    updated_at?: string;
  };
  
  export type IRole = {
    id?: number;
    description?: string;
    created_at?: string;
    updated_at?: string;
    pivot?: {
      user_id?: string;
      role_id?: number;
    };
  };
  