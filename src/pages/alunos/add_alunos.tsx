import { useForm } from "react-hook-form";
import { ModalDialog } from "../../components/modalComponent";
import { ToastInfo } from "../../components/ToastInfo";
import HAlunos from "../../hooks/alunos";

interface IAddAlunos {
    id: any
    getLisData: any
}

export default function AddAlunos({ id, getLisData }: IAddAlunos) {

    const apiAlunos = HAlunos();
    const { handleSubmit, register, reset, formState: { errors }, control } = useForm<any>({
        defaultValues: {
            nome: '',
            cpf: '',
            sexo: '',
            data_nascimento: '',
            email: '',
            renda_mensal: '',
        },
    })

    const add = async (formdata: any) => {
        await apiAlunos.store(formdata).then((resp: any) => {
            ToastInfo(resp);
            (window as any).$("#" + id).modal("hide");
            getLisData();
            reset();
        }).catch((error: any) => {
            (window as any).$(`#${id}`).modal('hide');
        })

    }

    return (
        <ModalDialog id={id} title="Cadastrar novo Aluno" form={true} onClick={handleSubmit(add)} buttonClose={[true, 'Cancelar']} buttonSend={[true, 'Salvar']}>

            <div className="mb-3">
                <label>Nome</label>
                <input type="text" autoComplete="off" {...register("nome", { required: true })} className="form-control" />
                {
                    errors.nome &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>
            <div className="mb-3">
                <label>CPF</label>
                <input type="text" autoComplete="off" {...register("cpf", { required: true })} className="form-control" />
                {
                    errors.cpf &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>
            <div className="mb-3">
                <label>Sexo</label>
                <select className="form-control" {...register("sexo", { required: true })} >
                    <option value="">Selecione...</option>
                    <option value="Masculino">Masculino</option>
                    <option value="Feminino">Feminino</option>
                </select>
                {
                    errors.sexo &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>
            <div className="mb-3">
                <label>Data Nascimento</label>
                <input type="date" autoComplete="off" {...register("data_nascimento", { required: true })} className="form-control" />
                {
                    errors.data_nascimento &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>
            <div className="mb-3">
                <label>email</label>
                <input type="email" autoComplete="off" {...register("email", { required: true })} className="form-control" />
                {
                    errors.email &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>

            <div className="mb-3">
                <label>Renda Mensal</label>
                <input autoComplete="off" {...register("renda_mensal")} className="form-control" />
            </div>
        </ModalDialog>
    );
}