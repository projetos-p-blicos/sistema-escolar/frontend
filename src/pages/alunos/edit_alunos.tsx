
import { ToastInfo } from "../../components/ToastInfo";
import { ModalDialog } from "../../components/modalComponent";
import HAlunos from "../../hooks/alunos";
import { useForm } from "react-hook-form";

interface IEditAlunos {
    id: any
    getLisData: any,
    firstData: any
}

export default function EditAlunos({ id, getLisData, firstData }: IEditAlunos) {

    const apiAlunos = HAlunos();
    const { handleSubmit, register, reset, formState: { errors }, control } = useForm<any>({
        defaultValues: {
            nome: firstData.nome,
            cpf: firstData.cpf,
            sexo: firstData.sexo,
            data_nascimento: firstData.data_nascimento,
            active: firstData.active ? 1 : 0,
            email: firstData.email,
            renda_mensal: firstData.renda_mensal
        },
        values: {
            nome: firstData.nome,
            cpf: firstData.cpf,
            sexo: firstData.sexo,
            data_nascimento: firstData.data_nascimento,
            active: firstData.active ? 1 : 0,
            email: firstData.email,
            renda_mensal: firstData.renda_mensal
        }
    })

    const edit = async (formdata: any) => {
        formdata.id = firstData.id;
        await apiAlunos.update(formdata).then((resp: any) => {
            ToastInfo(resp);
            (window as any).$("#" + id).modal("hide");
            getLisData();
            reset();
        }).catch((error: any) => {
            (window as any).$(`#${id}`).modal('hide');
        })

    }
    return (
        <ModalDialog id={id} title="Editar dados do Aluno" onClick={handleSubmit(edit)} buttonClose={[true, 'Cancelar']} buttonSend={[true, 'Editar']}>
            <div className="mb-3">
                <label>Nome</label>
                <input type="text" autoComplete="off" {...register("nome", { required: true })} className="form-control" />
                {
                    errors.nome &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>
            <div className="mb-3">
                <label>CPF</label>
                <input type="text" autoComplete="off" {...register("cpf", { required: true })} className="form-control" />
                {
                    errors.cpf &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>
            <div className="mb-3">
                <label>Sexo</label>
                <select className="form-control" {...register("sexo", { required: true })} >
                    <option value="">Selecione...</option>
                    <option value="Masculino">Masculino</option>
                    <option value="Feminino">Feminino</option>
                </select>
                {
                    errors.sexo &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>

            <div className="mb-3">
                <label >Status:</label>
                <select className="form-control" {...register('active')} >
                    <option value={1}>Ativo</option>
                    <option value={0}>Inativo</option>
                </select>
            </div>
            
            <div className="mb-3">
                <label>Data Nascimento</label>
                <input type="date" autoComplete="off" {...register("data_nascimento", { required: true })} className="form-control" />
                {
                    errors.data_nascimento &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>
            <div className="mb-3">
                <label>email</label>
                <input type="email" autoComplete="off" {...register("email", { required: true })} className="form-control" />
                {
                    errors.email &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>

            <div className="mb-3">
                <label>Renda Mensal</label>
                <input autoComplete="off" {...register("renda_mensal")} className="form-control" />
            </div>

        </ModalDialog>
    );
}