import TableComponent from "../../components/tableComponent";
import { ModalDialog } from "../../components/modalComponent";
import moment from "moment";

interface IViewAlunos {
    id: any
    firstData: any
}

export default function ViewAlunos({ id, firstData }: IViewAlunos) {

    return (
        <ModalDialog modalSize={'lg'} id={id} title="Detalhes do Aluno" onClick={() => { }} buttonClose={[true, 'Fechar']}>
            <span><strong>Nome:</strong> {firstData.nome}</span> <br />
            <span><strong>CPF:</strong>  {firstData.cpf} </span> <br />
            <span><strong>Sexo:</strong>  {firstData.sexo} </span> <br />
            <span><strong>E-mail:</strong>  {firstData.email} </span> <br />
            <span><strong>Renda Mensal:</strong>  {firstData.renda_mensal} </span> <br />
            <span><strong>Data Nascimento:</strong>  {firstData.data_nascimento} </span> <br />
            <span><strong>Data Cadastro:</strong>  {moment(firstData.created_at).format('DD/MM/YYYY')} </span> <br />


            <hr />

            <TableComponent colums={['#', 'Codigo Turma', 'data_inicio', 'data_fim',]}>
                {
                    firstData?.turmas?.map((d: any, k: any) => (
                        <tr key={k}>
                            <td>{d.id}</td>
                            <td>{d.codigo_turma}</td>
                            <td>{moment(d.data_inicio).format('DD/MM/YYYY')}</td>
                            <td>{moment(d.data_fim).format('DD/MM/YYYY')}</td>
                        </tr>
                    ))
                }
            </TableComponent>
        </ModalDialog>
    );
}