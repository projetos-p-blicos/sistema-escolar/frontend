import StaffPage from "../../components/staffPage";
import SvgVehicles from '../../assets/svg/vehicles.svg'
import CardSearchAndAdd from "../../components/cardSearchAndAdd";
import TableComponent from "../../components/tableComponent";
import { useEffect, useState } from 'react';
import HAlunos from "../../hooks/alunos";
import moment from "moment";
import AddAlunos from "./add_alunos";
import ViewAlunos from "./view_alunos";
import EditAlunos from "./edit_alunos";
import { FiEdit, FiFile } from "react-icons/fi";

export default function Alunos() {

    const apiAlunos = HAlunos();
    const [listData, setListData] = useState<any>();
    const [search, setSearch] = useState<String>('');
    const [loading, setLoading] = useState<boolean>(false);
    const [firstData, setFirstData] = useState<any>([]);


    const getLisData = async (numPage = 1, _search = search) => {
        setLoading(true)
        const params = {
            page: numPage,
            search: _search,
            showActive: 0
        }
        await apiAlunos.index(params).then((res: any) => {
            setListData(res)
        }).finally(() => { setLoading(false) })
    }

    useEffect(() => {
        getLisData();
    }, [])


    return (
        <>
            <StaffPage iconSvg={SvgVehicles} description='Alunos' name='Alunos' />

            <CardSearchAndAdd add={() => { (window as any).$(`#modalAdd`).modal('show') }} searchButton={() => { getLisData(1, search) }} searchValue={search} searchChange={(e: any) => { setSearch(e.target.value) }} />

            <div className="card">
                <h5 className="card-header">Todos Alunos</h5>
                <TableComponent PaginateFunction={getLisData} PaginateListData={listData} loading={loading} colums={['#', 'Nome', 'CPF', 'SEXO', 'Status', 'Data Nascimento', 'Cadastro', 'Ações']} >
                    {
                        listData &&
                        listData.data.map((d: any, k: any) => (
                            <tr key={k}>
                                <td>{d.id}</td>
                                <td>{d.nome}</td>
                                <td>{d.cpf}</td>
                                <td>{d.sexo}</td>
                                <td> <span className={`badge bg-label-${d.active == 1 ? 'success' : 'danger'} me-1`}>{d.active == 1 ? 'Ativo' : 'Inativo'}</span></td>
                                <td>{moment(d.data_nascimento).format('DD/MM/YYYY')}</td>
                                <td>{moment(d.created_at).format('DD/MM/YYYY')}</td>
                                <td>
                                    <div className="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                        <button onClick={() => { (window as any).$('#modalView').modal('show'); setFirstData(d) }} title='Visualizar' type="button" className="btn btn-primary"><FiFile /></button>
                                        <button onClick={() => { (window as any).$('#modalEdit').modal('show'); setFirstData(d) }} title='Editar' type="button" className="btn btn-primary"><FiEdit /></button>
                                    </div>
                                </td>
                            </tr>
                        ))
                    }
                </TableComponent>
            </div>

            <AddAlunos getLisData={getLisData} id='modalAdd' />
            <ViewAlunos firstData={firstData} id='modalView' />
            <EditAlunos firstData={firstData} getLisData={getLisData} id='modalEdit' />

        </>
    )
}