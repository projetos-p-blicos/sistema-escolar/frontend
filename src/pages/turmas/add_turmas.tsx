import { useForm } from "react-hook-form";
import { ModalDialog } from "../../components/modalComponent";
import { ToastInfo } from "../../components/ToastInfo";
import HTurmas from "../../hooks/turmas";

interface IAddTurma {
    id: any
    getLisData: any
}

export default function AddTurma({ id, getLisData }: IAddTurma) {

    const apiTurma = HTurmas();
    const { handleSubmit, register, reset, formState: { errors }, control } = useForm<any>({
        defaultValues: {
            codigo_turma: '',
            data_inicio: '',
            data_fim: '',
            quantidade_maxima_alunos: '',
        },
    })

    const add = async (formdata: any) => {
        await apiTurma.store(formdata).then((resp: any) => {
            ToastInfo(resp);
            (window as any).$("#" + id).modal("hide");
            getLisData();
            reset();
        }).catch((error: any) => {
            (window as any).$(`#${id}`).modal('hide');
        })

    }

    return (
        <ModalDialog id={id} title="Cadastrar nova Turma" form={true} onClick={handleSubmit(add)} buttonClose={[true, 'Cancelar']} buttonSend={[true, 'Salvar']}>
            <div className="mb-3">
                <label>Codigo Turma</label>
                <input type="text" autoComplete="off" {...register("codigo_turma", { required: true })} className="form-control" />
                {
                    errors.codigo_turma &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>

            <div className="mb-3">
                <label>Data Inicio</label>
                <input type="date" autoComplete="off" {...register("data_inicio", { required: true })} className="form-control" />
                {
                    errors.data_inicio &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>

            <div className="mb-3">
                <label>Data Fim</label>
                <input type="date" autoComplete="off" {...register("data_fim", { required: true })} className="form-control" />
                {
                    errors.data_fim &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>

            <div className="mb-3">
                <label>Quantidade Maxima Alunos</label>
                <input type="text" autoComplete="off" {...register("quantidade_maxima_alunos", { required: true })} className="form-control" />
                {
                    errors.quantidade_maxima_alunos &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>

        </ModalDialog>
    );
}