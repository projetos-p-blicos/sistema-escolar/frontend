import { useEffect, useState } from "react";
import TableComponent from "../../components/tableComponent";
import { ModalDialog } from "../../components/modalComponent";
import { useNavigate, useParams } from "react-router-dom";
import { FiPlus, FiSearch } from "react-icons/fi";
import HAlunos from "../../hooks/alunos";
import HTurmas from "../../hooks/turmas";
import { ToastInfo } from "../../components/ToastInfo";

export default function AddAlunosTurma() {

    const { id } = useParams();
    const navigate = useNavigate();
    const apiAlunos = HAlunos();
    const ApiTurma = HTurmas();
    const [selectedAlunos, setSelectedAlunos] = useState<any>([]);
    const [listDatahAlunos, setListDatahAlunos] = useState<any>();
    const [searchVehicle, setSearchAluno] = useState<string>('');
    const [firstTurma, setFirstTurma] = useState<any>([]);
    const [loading, setLoading] = useState<boolean>(false);


    const handleCheckboxClickAlunos = (alunoId: number, isChecked: boolean) => {
        // Se o checkbox está marcado e o ID do veículo não está na lista de selecionados, adicione-o.
        if (isChecked && !selectedAlunos.includes(alunoId)) {
            setSelectedAlunos([...selectedAlunos, alunoId]);
        }
        // Se o checkbox não está marcado e o ID do veículo está na lista de selecionados, remova-o.
        else if (!isChecked && selectedAlunos.includes(alunoId)) {
            setSelectedAlunos(selectedAlunos.filter((id: any) => id !== alunoId));
        }
    }


    const getListDataAlunos = async (numPage = 1, _search = searchVehicle) => {
        const params = {
            page: numPage,
            paginate: 10,
            search: _search,
        }
        await apiAlunos.index(params).then((res) => {
            setListDatahAlunos(res)
        }).finally(() => {
            setLoading(false)
        });
    }


    const getFirstDataTurma = async () => {
        await ApiTurma.index({ id: id }).then((res) => {
            setFirstTurma(res.data[0]);
            const AlunosId = res.data[0].alunos.map((item: any) => item.id);
            setSelectedAlunos(AlunosId);

        })
    }


    const upAlunosTurma = async () => {
        const datasend = {
            turma_id: firstTurma.id,
            alunos: selectedAlunos
        }
        await ApiTurma.addAlunoTurma(datasend).then((res: any) => {
            ToastInfo(res)
        }).catch(err => {
            console.log(err)
        }).finally(() => {
            getFirstDataTurma();
            (window as any).$('#addAlunosTurma').modal('hide');
        })
    }


    useEffect(() => {
        getFirstDataTurma();
        getListDataAlunos();
    }, [])


    return (
        <div className='card' style={{ padding: 30, }}>

            <div className="row">
                <div className="col-md-8" >
                    <strong>Atualizar Alunos da Turma:  </strong>
                    {firstTurma.codigo_turma}
                </div>
                <div className="col-md-4" style={{ display: 'flex', justifyContent: 'end' }}>
                    <button onClick={() => { (window as any).$('#addAlunosTurma').modal('show') }} className="btn btn-primary btn-sm" ><FiPlus /></button>
                </div>
            </div>

            <TableComponent colums={['#', 'Nome', 'CPF', 'Email']} size="sm">
                {
                    firstTurma?.alunos &&
                    firstTurma?.alunos?.map((d: any, k: any) => (
                        <tr key={k}>
                            <td>{d.id}</td>
                            <td>{d.nome}</td>
                            <td>{d.cpf}</td>
                            <td>{d.email}</td>
                        </tr>
                    ))
                }
            </TableComponent>


            <ModalDialog id="addAlunosTurma" modalDialog="centered" onClick={(upAlunosTurma)} buttonClose={[true, 'Cancelar']} buttonSend={[true, 'Atualizar']} title="Adicionar Veículos" >

                <div>
                    <div className="input-group mb-3">
                        <input type="text" value={searchVehicle} onChange={(e: any) => { setSearchAluno(e.target.value) }} className="form-control" placeholder="Pesquisar..." />
                        <button onClick={() => getListDataAlunos(1, searchVehicle)} type='button' className="btn btn-primary"><FiSearch /></button>
                    </div>
                </div>

                <TableComponent size='sm' PaginateFunction={getListDataAlunos} PaginateListData={listDatahAlunos} colums={['selecione', '#', 'Nome', 'CPF', 'Email']}>
                    {
                        listDatahAlunos &&
                        listDatahAlunos.data.map((d: any, k: any) => (
                            <tr key={k}>
                                <td>
                                    <div className="form-check form-switch">
                                        <input
                                            className="form-check-input"
                                            type="checkbox"
                                            id={`vehicle-checkbox-${d.id}`}
                                            onChange={(e) => handleCheckboxClickAlunos(d.id, e.target.checked)}
                                            checked={selectedAlunos.includes(d.id)}
                                        />

                                    </div>
                                </td>
                                <td>{d.id}</td>
                                <td>{d.nome}</td>
                                <td>{d.cpf}</td>
                                <td>{d.email}</td>
                            </tr>
                        ))
                    }
                </TableComponent>
            </ModalDialog >


            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <button onClick={() => { navigate('/turmas') }} className='btn btn-primary'>Voltar</button>
            </div>

        </div>
    );
}