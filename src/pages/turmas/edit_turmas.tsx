
import { ToastInfo } from "../../components/ToastInfo";
import { ModalDialog } from "../../components/modalComponent";
import { useForm } from "react-hook-form";
import HTurmas from "../../hooks/turmas";

interface IEditTurma {
    id: any
    getLisData: any,
    firstData: any
}


export default function EditTurma({ id, getLisData, firstData }: IEditTurma) {

    const apiTurma = HTurmas();
    const { handleSubmit, register, reset, formState: { errors }, control } = useForm({
        defaultValues: {
            codigo_turma: firstData.codigo_turma,
            data_inicio: firstData.data_inicio,
            data_fim: firstData.data_fim,
            quantidade_maxima_alunos: firstData.quantidade_maxima_alunos,

        },
        values: {
            codigo_turma: firstData.codigo_turma,
            data_inicio: firstData.data_inicio,
            data_fim: firstData.data_fim,
            quantidade_maxima_alunos: firstData.quantidade_maxima_alunos,
        }
    })

    const edit = async (formdata: any) => {
        formdata.id = firstData.id;
        await apiTurma.update(formdata).then((resp: any) => {
            ToastInfo(resp);
            (window as any).$("#" + id).modal("hide");
            getLisData();
            reset();
        }).catch((error: any) => {
            (window as any).$(`#${id}`).modal('hide');
        })

    }
    return (
        <ModalDialog id={id} title="Editar Turma" onClick={handleSubmit(edit)} buttonClose={[true, 'Cancelar']} buttonSend={[true, 'Editar']}>
            <div className="mb-3">
                <label>Codigo Turma</label>
                <input type="text" autoComplete="off" {...register("codigo_turma", { required: true })} className="form-control" />
                {
                    errors.codigo_turma &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>

            <div className="mb-3">
                <label>Data Inicio</label>
                <input type="date" autoComplete="off" {...register("data_inicio", { required: true })} className="form-control" />
                {
                    errors.data_inicio &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>

            <div className="mb-3">
                <label>Data Fim</label>
                <input type="date" autoComplete="off" {...register("data_fim", { required: true })} className="form-control" />
                {
                    errors.data_fim &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>

            <div className="mb-3">
                <label>Quantidade Maxima Alunos</label>
                <input type="text" autoComplete="off" {...register("quantidade_maxima_alunos", { required: true })} className="form-control" />
                {
                    errors.quantidade_maxima_alunos &&
                    <span style={{ fontSize: 12, color: 'red' }}>Campo obrigatório</span>
                }
            </div>

        </ModalDialog>
    );
}