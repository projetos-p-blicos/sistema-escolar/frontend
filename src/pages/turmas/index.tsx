import CardSearchAndAdd from "../../components/cardSearchAndAdd";
import TableComponent from "../../components/tableComponent";
import HTurmas from "../../hooks/turmas";
import { useEffect, useState } from 'react';
import StaffPage from "../../components/staffPage";
import SvgVehicles from '../../assets/svg/vehicles.svg'
import moment from "moment";
import AddTurma from "./add_turmas";
import EditTurma from "./edit_turmas";
import ViewTurma from "./view_turmas";
import { FiEdit, FiFile, FiUsers } from "react-icons/fi";
import { useNavigate } from "react-router-dom";

export default function Turmas() {

    const apiTurma = HTurmas();
    const navigate = useNavigate();
    const [listData, setListData] = useState<any>();
    const [search, setSearch] = useState<String>('');
    const [loading, setLoading] = useState<boolean>(false);
    const [firstData, setFirstData] = useState<any>([]);


    const getLisData = async (numPage = 1, _search = search) => {
        setLoading(true)
        const params = {
            page: numPage,
            search: _search,
            showActive: 0
        }
        await apiTurma.index(params).then((res: any) => {
            setListData(res)
        }).finally(() => { setLoading(false) }).catch(() => {

        })
    }

    useEffect(() => {
        getLisData();
    }, [])



    return (
        <>
            <StaffPage iconSvg={SvgVehicles} description='Turmas' name='Turmas' />

            <CardSearchAndAdd add={() => { (window as any).$(`#modalAdd`).modal('show') }} searchButton={() => { getLisData(1, search) }} searchValue={search} searchChange={(e: any) => { setSearch(e.target.value) }} />

            <div className="card">
                <h5 className="card-header">Todos Alunos</h5>
                <TableComponent PaginateFunction={getLisData} PaginateListData={listData} loading={loading} colums={['#', 'codigo_turma', 'data_inicio', 'data_fim', 'quantidade_maxima_alunos', 'Cadastro', 'Ações']} >
                    {
                        listData &&
                        listData.data.map((d: any, k: any) => (
                            <tr key={k}>
                                <td>{d.id}</td>
                                <td>{d.codigo_turma}</td>
                                <td>{moment(d.data_inicio).format('DD/MM/YYYY')}</td>
                                <td>{moment(d.data_fim).format('DD/MM/YYYY')}</td>
                                <td>{d.quantidade_maxima_alunos}</td>
                                <td>{moment(d.created_at).format('DD/MM/YYYY')}</td>
                                <td>
                                    <div className="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                        <button onClick={() => { (window as any).$('#modalView').modal('show'); setFirstData(d) }} title='Visualizar' type="button" className="btn btn-primary"><FiFile /></button>
                                        <button onClick={() => { (window as any).$('#modalEdit').modal('show'); setFirstData(d) }} title='Editar' type="button" className="btn btn-primary"><FiEdit /></button>
                                        <button onClick={() => { navigate(`/turmas/edit/${d.id}`) }} title='Editar' type="button" className="btn btn-primary"><FiUsers /></button>
                                    </div>
                                </td>
                            </tr>
                        ))
                    }
                </TableComponent>
            </div>

            <AddTurma getLisData={getLisData} id='modalAdd' />
            <ViewTurma firstData={firstData} id='modalView' />
            <EditTurma firstData={firstData} getLisData={getLisData} id='modalEdit' />

        </>
    )
}