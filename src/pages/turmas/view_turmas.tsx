import TableComponent from "../../components/tableComponent";
import { ModalDialog } from "../../components/modalComponent";
import moment from "moment";

interface IViewTurma {
    id: any
    firstData: any
}

export default function ViewTurma({ id, firstData }: IViewTurma) {

    return (
        <ModalDialog modalSize={'xl'} id={id} title="Detalhes da Turma" onClick={() => { }} buttonClose={[true, 'Fechar']}>
            <span><strong>Codigo Turma:</strong> {firstData.codigo_turma}</span> <br />
            <span><strong>Data Inicio:</strong>  {firstData.data_inicio} </span> <br />
            <span><strong>Data Fim:</strong>  {firstData.data_fim} </span> <br />
            <span><strong>Data Cadastro:</strong>  {moment(firstData.created_at).format('DD/MM/YYYY')} </span> <br />

            <TableComponent colums={['#', 'Nome', 'CPF', 'SEXO', 'Status', 'Data Nascimento']}>
                {
                    firstData?.alunos?.map((d: any, k: any) => (
                        <tr key={k}>
                            <td>{d.id}</td>
                            <td>{d.nome}</td>
                            <td>{d.cpf}</td>
                            <td>{d.sexo}</td>
                            <td> <span className={`badge bg-label-${d.active == 1 ? 'success' : 'danger'} me-1`}>{d.active == 1 ? 'Ativo' : 'Inativo'}</span></td>
                            <td>{moment(d.data_nascimento).format('DD/MM/YYYY')}</td>
                        </tr>
                    ))
                }
            </TableComponent>

        </ModalDialog>
    );
}