import { Route, Routes } from "react-router-dom";
import LayoutDefaut from "./layouts/default";


import Alunos from "./pages/alunos";
import Turmas from "./pages/turmas";
import AddAlunosTurma from "./pages/turmas/add_alunos_turma";

export default function Rotas() {
    return (
        <Routes>

            <Route element={<LayoutDefaut />}>
                <Route path="/alunos" element={<Alunos />} />
                <Route path="/turmas" element={<Turmas />} />

                <Route path="turmas/edit/:id" element={<AddAlunosTurma />} />
            </Route>

        </Routes >
    )

}

