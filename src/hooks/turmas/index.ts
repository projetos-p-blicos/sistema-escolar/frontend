import Api from "../../services/api";

const HTurmas = () => ({
    index: async (data: any) => {
        const response = await Api.get("/turmas", {
            params: data,
        });
        return response.data;
    },
    store: async (data: any) => {
        const response = await Api.post("/turmas", data);
        return response.data;
    },
    update: async (data: any) => {
        try {
            const response = await Api.put(`/turmas/${data.id}`, data);
            return response.data;
        } catch (error) {
            throw error;
        }
    },
    delete: async (id: any) => {
        try {
            const response = await Api.delete(`/turmas/${id}`);
            return response.data;
        } catch (error) {
            throw error;
        }
    },
    addAlunoTurma: async (data: any) => {
        const response = await Api.post("/turmas/add_aluno_turma", data);
        return response.data;
    },
});

export default HTurmas;
