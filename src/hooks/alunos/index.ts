import Api from "../../services/api";

const HAlunos = () => ({
    index: async (data: any) => {
        const response = await Api.get("/alunos", {
            params: data,
        });
        return response.data;
    },
    store: async (data: any) => {
        const response = await Api.post("/alunos", data);
        return response.data;
    },

    update: async (data: any) => {
        try {
            const response = await Api.put(`/alunos/${data.id}`, data);
            return response.data;
        } catch (error) {
            throw error;
        }
    },
    delete: async (id: any) => {
        try {
            const response = await Api.delete(`/alunos/${id}`);
            return response.data;
        } catch (error) {
            throw error;
        }
    },

    
});

export default HAlunos;
